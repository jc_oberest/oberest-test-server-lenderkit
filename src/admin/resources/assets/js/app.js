//module-rbam-lines-begin
require('../vendor/lk-rbam/js/extenders.js'); // RBAM module relation.
//module-rbam-lines-end

require('../vendor/lk-core/js/bootstrap.js');
require('../vendor/lk-core/js/helpers');
require('../vendor/lk-core/js/init');


//module-gdpr-lines-begin
require('../vendor/lk-gdpr/js/extenders.js');
//module-gdpr-lines-end

//module-investor-categorization-lines-begin
require('../vendor/lk-investor-categorization/js/init.js');
//module-investor-categorization-lines-end

//module-branding-lines-begin
require('../vendor/lk-branding/js/extenders.js');
//module-branding-lines-end

//module-themes-lines-begin
window.Vue.component('ThemeCustomizer', require('../vendor/lk-themes/js/components/Themes/ThemeCustomizer.vue').default); // Themes module relation.
//module-themes-lines-end

const app = new Vue({
  el: '#app',
});
