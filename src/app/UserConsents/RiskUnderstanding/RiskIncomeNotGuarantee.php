<?php
declare(strict_types=1);


namespace App\UserConsents\RiskUnderstanding;

/**
 * Class RiskIncomeNotGuarantee
 *
 * @package App\UserConsents\RiskUnderstanding
 */
class RiskIncomeNotGuarantee extends BaseRiskUnderstandingConsent
{
    /**
     * @var string
     */
    protected $key = 'risk_income_not_guarantee';
}
