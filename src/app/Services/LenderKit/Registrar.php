<?php

declare(strict_types=1);

namespace App\Services\LenderKit;

use Illuminate\Contracts\Foundation\Application;

/**
 * Class Registrar
 *
 * @package App\Services\LenderKit
 */
abstract class Registrar
{
    /**
     * App
     *
     * @var Application
     */
    protected $app;

    /**
     * Registrar constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Map
     *
     * @return array
     */
    abstract protected function map(): array;

    /**
     * Register
     */
    public function bind()
    {
        foreach ($this->map() as $abstract => $concrete) {
            $this->app->bind($abstract, $concrete);
        }
    }
}
